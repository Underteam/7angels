﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour {

    public DialogueSystem.Dialogue dialogue;

    public List<Text> answers;

    public Messenger messenger;

    public RectTransform canvas;

    public RectTransform answersPanel;

    private bool autoContinue = true;

    private Vector2 answersHidePos = Vector2.zero;
    private Vector2 answersHalfShowPos = new Vector2(0, 290);
    private Vector2 answersShowPos = new Vector2(0, 560);
    private Vector2 targetAnswersPos;

    private float messengerHeight;

    private RectTransform messengerRT;

	// Use this for initialization
	void Start () {

        Debug.Log(canvas.rect.height + " " + canvas.GetComponent<CanvasScaler>().scaleFactor + " " +  Screen.height + " " + answersPanel.position + " " + answersPanel.localPosition + " " + answersPanel.anchoredPosition);

        messengerRT = (messenger.transform as RectTransform);
        messengerHeight = messengerRT.sizeDelta.y;

        dialogue.Restart();
        PreApply();

        GMATools.Common.EventManager.Instance().AddListener("GoToDialogue", (o) =>
        {
            var entry = o as DialogueSystem.EntryNode;
            if (entry != null) dialogue = entry.graph as DialogueSystem.Dialogue;
            dialogue.Restart(entry);
        });
	}
	
	// Update is called once per frame
	void Update () {

        answersPanel.anchoredPosition = Vector2.Lerp(answersPanel.anchoredPosition, targetAnswersPos, 10 * Time.deltaTime);

        if(Mathf.Abs(answersPanel.anchoredPosition.y - targetAnswersPos.y) > 0.01f)
        {
            messengerRT.sizeDelta = new Vector2(0, messengerHeight + (answersShowPos.y - answersPanel.anchoredPosition.y));
        }

	}

    private void PreApply ()
    {
        for (int i = 0; i < answers.Count; i++) answers[i].transform.parent.gameObject.SetActive(false);

        answersPanel.anchoredPosition = answersHidePos;
        targetAnswersPos = answersHidePos;

        messengerRT.sizeDelta = new Vector2(0, messengerHeight + (answersShowPos.y - answersPanel.anchoredPosition.y));

        if (!dialogue.IsReady()) return;

        messenger.StartTyping();

        Invoke("Apply", 2f);
    }

    void Apply ()
    {
        //Debug.Log("Current is " + dialogue.current, dialogue.current);

        messenger.ReceiveMessage(dialogue.current.text, dialogue.current.image);

        for (int i = 0; i < dialogue.current.answers.Count; i++)
        {
            answers[i].transform.parent.gameObject.SetActive(true);
            answers[i].text = dialogue.current.answers[i].text;
        }

        if(dialogue.current.answers.Count == 0)
        {
            if (autoContinue)
            {
                Select(0);
                targetAnswersPos = answersHidePos;
            }
            else
            {
                answers[0].transform.parent.gameObject.SetActive(true);
                answers[0].text = "Далее";
                targetAnswersPos = answersHalfShowPos;
            }
        }
        else if(dialogue.current.answers.Count < 3)
        {
            targetAnswersPos = answersHalfShowPos;
        }
        else
        {
            targetAnswersPos = answersShowPos;
        }
    }

    public void Select (int i)
    {
        if(i >= 0 && i < dialogue.current.answers.Count)
        {
            messenger.SendMessage(dialogue.current.answers[i].text);
        }

        for (int j = 0; j < answers.Count; j++) answers[j].transform.parent.gameObject.SetActive(false);
        targetAnswersPos = answersHidePos;

        dialogue.Select(i);

        Invoke("PostApply", 1f);
    }

    private void PostApply ()
    {
        PreApply();
    }
}
