﻿using UnityEngine;
using System.Collections;

public class MonoSingleton<T> 	: MonoBehaviour 
						where T : MonoBehaviour
{

	private static T instance;

	public static T Instance() {

		if (instance == null) {
			instance = GameObject.FindObjectOfType<T>();
			//Debug.Log ("Find " + typeof(T) + " " + (instance != null), instance.gameObject);
			if(instance == null) {
				GameObject go = new GameObject(typeof(T).ToString());
				instance = go.AddComponent<T>();
			}
			if (!(instance as MonoSingleton<T>).inited) (instance as MonoSingleton<T>).Init ();
		}

		return instance;
	}

	// Use this for initialization
	protected void Awake () {
	
		//Debug.Log ("MonoSingleton " + typeof(T) + " " + (instance == null), gameObject);

        if (instance == null)
        {
            instance = this as T;
            if (!inited) Init();
        }
        else if(instance != this)
        {
            Destroy(this);
        }
	}

	protected bool inited = false;
	protected virtual void Init() {
		inited = true;
	}
}
