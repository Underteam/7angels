﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Messenger : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Message receivedMessagePrefab;
    public Message sentMessagePrefab;

    public ScrollRect scroller;

    private bool updatePosition = true;

    private Message typingMessage;

    private bool dragging;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {

        if (!dragging && scroller.verticalNormalizedPosition < 0.05f) updatePosition = true;
        if (updatePosition) scroller.verticalNormalizedPosition = Mathf.Lerp(scroller.verticalNormalizedPosition, 0f, 10f * Time.deltaTime);
    }

    public void StartTyping ()
    {
        if (typingMessage != null) return;
        var m = Clone(receivedMessagePrefab.transform).GetComponent<Message>();
        m.gameObject.SetActive(true);
        m.lblMessage.text = "...";
        m.lblTime.text = "typing";
        typingMessage = m;
    }

    public void StopTyping()
    {
        if (typingMessage != null) Destroy(typingMessage);
    }

    public void ReceiveMessage (string message, Sprite image = null)
    {
        Debug.Log("Received: " + message + " " + (image != null), image);
        var m = typingMessage;
        if(m == null) m = Clone(receivedMessagePrefab.transform).GetComponent<Message>();
        m.gameObject.SetActive(true);
        m.Apply(message, image);
        //m.lblMessage.text = message;
        m.lblTime.text = DateTime.Now.ToShortTimeString();

        typingMessage = null;
    }

    public void SendMessage (string message, Sprite image = null)
    {
        Debug.Log("Sent: " + message);
        var m = Clone(sentMessagePrefab.transform).GetComponent<Message>();
        m.gameObject.SetActive(true);
        m.lblMessage.text = message;
        m.lblTime.text = "";

        updatePosition = true;
    }

    private Transform Clone (Transform prefab)
    {
        var clone = Instantiate(prefab);

        clone.SetParent(prefab.parent);
        clone.localScale = prefab.localScale;

        return clone;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        updatePosition = false;
        dragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        dragging = false;
    }
}
