﻿//C# Unity event manager that uses strings in a hashtable over delegates and events in order to
//allow use of events without knowing where and when they're declared/defined.
//by Billy Fletcher of Rubix Studios
using UnityEngine;
using System.Collections;

namespace GMATools {

	namespace Common {

		struct MyEvent {

			public string name;
			public object data;
		}

		public class EventManager
		{

			public delegate void EventHandler(object data);

			public bool LimitQueueProcesing = false;
			public float QueueProcessTime = 0.0f;
			
			private static EventManager instance = null;

			public static EventManager Instance () {

				if (instance == null) 
				{
					instance = new EventManager();

					Updater.CreateUpdater(instance.Update, "EventManagerUpdater");
				}
				
				return instance;
			}

            public EventManager ()
            {
                //Debug.LogWarning("Creating EventHandler instance");
            }

            public void ClearAll ()
            {
                instance = new EventManager();
            }

			private Hashtable m_listenerTable = new Hashtable();
			private Hashtable m_listenerTable2 = new Hashtable();
			private static Queue queue = new Queue();
			private Queue m_eventQueue = Queue.Synchronized (queue);
			
			
			//Add a listener to the event manager that will receive any events of the supplied event name.
			public bool AddListener(string eventName, GameObject listener)
			{
				if (listener == null || eventName == null)
				{
					Debug.Log("Event Manager: AddListener failed due to no listener or event name specified.");
					return false;
				}
				
				if (!m_listenerTable.ContainsKey(eventName))
					m_listenerTable.Add(eventName, new ArrayList());
				
				ArrayList listenerList = m_listenerTable[eventName] as ArrayList;
				if (listenerList.Contains(listener))
				{
					Debug.Log("Event Manager: Listener: " + listener.GetType().ToString() + " is already in list for event: " + eventName);
					return false; //listener already in list
				}

				listenerList.Add(listener);
				return true;
			}

			public bool AddListener(string eventName, EventHandler listener)
			{

				if (listener == null || eventName == null)
				{
					Debug.Log("Event Manager: AddListener failed due to no listener or event name specified.");
					return false;
				}
				
				if (!m_listenerTable2.ContainsKey(eventName)) {
					m_listenerTable2.Add(eventName, new ArrayList());
				}
				
				ArrayList listenerList = m_listenerTable2[eventName] as ArrayList;
				if (listenerList.Contains(listener))
				{
					Debug.Log("Event Manager: Listener: " + listener.GetType().ToString() + " is already in list for event: " + eventName);
					return true; //listener already in list
				}

				listenerList.Add(listener);
				return true;
			}

			//Remove a listener from the subscribed to event.
			public bool DetachListener(GameObject listener, string eventName)
			{
				if (!m_listenerTable.ContainsKey(eventName))
					return false;
				
				ArrayList listenerList = m_listenerTable[eventName] as ArrayList;
				if (!listenerList.Contains(listener))
					return false;
				
				listenerList.Remove(listener);
				return true;
			}

			//Remove a listener from the subscribed to event.
			public bool DetachListener(EventHandler listener, string eventName)
			{
				if (!m_listenerTable2.ContainsKey(eventName))
					return false;
				
				ArrayList listenerList = m_listenerTable2[eventName] as ArrayList;
				if (!listenerList.Contains(listener))
					return false;
				
				listenerList.Remove(listener);
				return true;
			}

			public void DetachListeners(string eventName) {
			
				if (m_listenerTable.ContainsKey(eventName)) m_listenerTable.Remove(eventName);

				if (m_listenerTable2.ContainsKey(eventName)) m_listenerTable2.Remove(eventName);
			}

			public bool TriggerEvent(string evtName) {
			
				return TriggerEvent (evtName, null);
			}

			//Trigger the event instantly, this should only be used in specific circumstances,
			//the QueueEvent function is usually fast enough for the vast majority of uses.
			public bool TriggerEvent(string evtName, object data)
			{

				bool result = false;

				string eventName = evtName;

				if (m_listenerTable.ContainsKey(eventName)) {
					ArrayList listenerList = m_listenerTable[eventName] as ArrayList;
					foreach (GameObject listener in listenerList)
					{
						Debug.Log("Trigger event " + evtName + " at " + listener.name);
						listener.SendMessage(eventName, data, SendMessageOptions.DontRequireReceiver);
					}
					result = true;
				}

				if (m_listenerTable2.ContainsKey(eventName)) {
					/*bool res = false;
					int c = 0;
					while(!res && c < 10) {
						try {
							ArrayList listenerList2 = m_listenerTable2[eventName] as ArrayList;
							foreach (EventHandler listener in listenerList2)
							{
								Debug.Log("Found listener " + c);
								listener(data);
							}
							result = true;
							res = true;
						} catch {
							Debug.LogError("Error while triggering event");
						}
						c++;
					}
					if(!res) Debug.LogError("!!!");//*/

					ArrayList listenerList2 = m_listenerTable2[eventName] as ArrayList;
					foreach (EventHandler listener in listenerList2)
					{
						//Debug.Log("Trigger event " + evtName + " at " + listener);
						listener(data);
					}
					result = true;
				}

				if (!result)
				{
					Debug.Log("Event Manager: Event \"" + eventName + "\" triggered has no listeners!");
				}

				return result;
			}
			
			//Inserts the event into the current queue.
			public bool QueueEvent(string evtName, object data)
			{

				if (!m_listenerTable.ContainsKey(evtName) && !m_listenerTable2.ContainsKey(evtName))
				{
					Debug.Log("EventManager: QueueEvent failed due to no listeners for event: " + evtName);
					return false;
				}

				MyEvent ev = new MyEvent ();
				ev.name = evtName;
				ev.data = data;

				m_eventQueue.Enqueue(ev);

				return true;
			}
			
			//Every update cycle the queue is processed, if the queue processing is limited,
			//a maximum processing time per update can be set after which the events will have
			//to be processed next update loop.
			public void Update()
			{
				float timer = 0.0f;
				while (m_eventQueue.Count > 0)
				{

					if (LimitQueueProcesing)
					{
						if (timer > QueueProcessTime)
							return;
					}

					if(m_eventQueue == null) Debug.LogError("??????");

					MyEvent ev = (MyEvent)m_eventQueue.Dequeue();

					try {

						TriggerEvent(ev.name, ev.data);

					} catch (System.Exception e){

						Debug.LogError("Error while Dequeue");
						Debug.Log("Event: " + ev.name);
						//Debug.Log("Data: " + (string)ev.data);
						Debug.Log("Exception " + e.Source);
						Debug.Log("Exception " + e.ToString());
						Debug.Log("Count " + m_eventQueue.Count);//*/

						Time.timeScale = 0.0f;
						//ClientSide.GameController.Instance().SpecialFunction(ev.name, ev.data);
					}
					
					if (LimitQueueProcesing)
						timer += Time.deltaTime;
				}
			}

			~EventManager() {

				m_listenerTable.Clear();
				m_listenerTable2.Clear();
				m_eventQueue.Clear();
				instance = null;
			}
		}
	}
}