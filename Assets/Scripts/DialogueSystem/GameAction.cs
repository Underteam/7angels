﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DialogueSystem
{
    [CreateAssetMenu(menuName = "DialogueSystem/GameAction")]
    public sealed class GameAction : ScriptableObject
    {
        public string actionName;

        public bool useEvents;

        public bool useParameters;

        public string GetPropertyName()
        {
            //if (useParameters) return "parameters";
            //else
                return "";
        }
    }
}
