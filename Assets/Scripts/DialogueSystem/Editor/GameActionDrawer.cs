﻿using UnityEditor;
using UnityEngine;
using GameAction = DialogueSystem.GameAction;

namespace DialogueSystemEditor
{
    // prefab override logic works on the entire property.
    [CustomPropertyDrawer(typeof(DialogueSystem.GameAction))]
    public class GameActionDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();

            // Store old indent level and set it to 0, the PrefixLabel takes care of it

            position = EditorGUI.PrefixLabel(position, label);

            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            Rect buttonRect = position;
            //buttonRect.width = 160;

            string buttonLabel = "Select";
            DialogueSystem.GameAction currentAction = property.objectReferenceValue as DialogueSystem.GameAction;
            if (currentAction != null) buttonLabel = currentAction.actionName;
            if (GUI.Button(buttonRect, buttonLabel))
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("None"), currentAction == null, () => Apply(property, null));
                string[] guids = AssetDatabase.FindAssets("t:GameAction");
                for (int i = 0; i < guids.Length; i++)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                    DialogueSystem.GameAction action = AssetDatabase.LoadAssetAtPath(path, typeof(DialogueSystem.GameAction)) as DialogueSystem.GameAction;
                    if (action != null)
                    {
                        GUIContent content = new GUIContent(action.actionName);
                        string[] nameParts = action.name.Split(' ');
                        if (nameParts.Length > 1) content.text = nameParts[0] + "/" + action.name.Substring(nameParts[0].Length + 1);
                        menu.AddItem(content, action == currentAction, () => Apply(property, action));
                    }
                }
                menu.ShowAsContext();
            }

            ////EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.Space();
            //EditorGUILayout.Space();
            //EditorGUILayout.Space();

            //position.y += 20;

            ////position.x += buttonRect.width + 4;
            ////position.width -= buttonRect.width + 4;
            //EditorGUI.ObjectField(position, property, typeof(GameAction), GUIContent.none);
            ////EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        private void Apply(SerializedProperty property, DialogueSystem.GameAction action)
        {
            property.objectReferenceValue = action;
            property.serializedObject.ApplyModifiedProperties();
            property.serializedObject.Update();
        }
    }
}