﻿using System.Collections;
using System.Collections.Generic;
using DialogueSystem;
using UnityEngine;
using XNodeEditor;

namespace DialogueSystemEditor
{
    [CustomNodeGraphEditor(typeof(Dialogue))]
    public class DialogueEditor : NodeGraphEditor
    {
        public override string GetNodeMenuName(System.Type type)
        {
            if (type.Namespace == "DialogueSystem") return base.GetNodeMenuName(type).Replace("DialogueSystem/", "");
            else return null;
        }

        public override void OnGUI()
        {
            //GUILayout.BeginArea(new Rect(0, 0, Screen.width, 0), GUI.skin.FindStyle("toolbar"));
            //GUILayout.BeginHorizontal();

            //if (GUILayout.Button("File", GUI.skin.FindStyle("toolbarDropdown"), GUILayout.Width(50)))
            //{
            //}

            //GUILayout.EndHorizontal();
            //GUILayout.EndArea();

            //base.OnGUI();
        }
    }
}