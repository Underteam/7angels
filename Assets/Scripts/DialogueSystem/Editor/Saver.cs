﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace DialogueSystem
{
    class Saver : EditorWindow
    {
        [MenuItem("Window/Dialogue System/Saver")]
        public static void ShowWindow()
        {
            //EditorWindow.GetWindow(typeof(Saver));

            var window = GetWindow<Saver>();
            window.minSize = new Vector2(400, 200);
            window.titleContent = new GUIContent("Dialogue System");
            //window.position = new Rect(0, 0, 180, 80);
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            DrawToolStrip();
            GUILayout.EndHorizontal();

            GUILayout.TextArea("");
        }

        void DrawToolStrip()
        {
            if (GUILayout.Button("File", EditorStyles.toolbarDropDown))
            {
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Export"), false, () => { Export(); });
                menu.AddItem(new GUIContent("Import"), false, () => { Import(); });
                menu.AddItem(new GUIContent("Add port"), false, () => { AddPort(); });
                menu.DropDown(new Rect(5, 0, 0, 17));

                //if (Selection.activeGameObject != null)
                //    toolsMenu.AddItem(new GUIContent("Optimize Selected"), false, OnTools_OptimizeSelected);
                //else
                //    toolsMenu.AddDisabledItem(new GUIContent("Optimize Selected"));
                //toolsMenu.AddSeparator("");
                //toolsMenu.AddItem(new GUIContent("Help..."), false, OnTools_Help);
                //// Offset menu from right of editor window
                //toolsMenu.DropDown(new Rect(Screen.width - 216 - 40, 0, 0, 16));
                EditorGUIUtility.ExitGUI();
            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Tools", EditorStyles.toolbarButton))
            {
                OnMenu_Create();
                EditorGUIUtility.ExitGUI();
            }
        }

        private void AddPort ()
        {

        }

        void OnMenu_Create()
        {
            // Do something!
        }

        void OnTools_OptimizeSelected()
        {
            // Do something!
        }

        void OnTools_Help()
        {
            Help.BrowseURL("http://example.com/product/help");
        }

        private void Export ()
        {
            string path = EditorUtility.SaveFilePanel("Save as...", "", "", "");
            Debug.Log(path);

            List<List<Node>> _dialogues = new List<List<Node>>();

            List<List<DialogueNodeBase>> dialogues = new List<List<DialogueNodeBase>>();

            string[] guids = AssetDatabase.FindAssets("t:Dialogue");
            for (int i = 0; i < guids.Length; i++)
            {
                path = AssetDatabase.GUIDToAssetPath(guids[i]);
                Dialogue dialogue = AssetDatabase.LoadAssetAtPath(path, typeof(Dialogue)) as Dialogue;
                if (dialogue != null)
                {
                    List<DialogueNodeBase> nodes = new List<DialogueNodeBase>();
                    List<Node> _nodes = new List<Node>();
                    Debug.Log("Found " + dialogue, dialogue);
                    for (int j = 0; j < dialogue.nodes.Count; j++)
                    {
                        nodes.Add(dialogue.nodes[j] as DialogueNodeBase);
                        if (dialogue.nodes[j] is DialogueNode)
                        {
                            DialogueNode node = dialogue.nodes[j] as DialogueNode;
                            Node n = new Node();
                            n.id = node.GetInstanceID();
                            n.text = node.text;
                            for (int k = 0; k < node.answers.Count; k++) n.answers.Add(node.answers[k].text);
                            _nodes.Add(n);
                        }
                    }
                    dialogues.Add(nodes);
                    _dialogues.Add(_nodes);
                }
            }

            Debug.Log("Found " + _dialogues.Count + " dialogues");
            for(int i = 0; i < _dialogues.Count; i++)
            {
                var d = _dialogues[i];
                for(int j = 0; j < d.Count; j++)
                {
                    Debug.Log(d[j].id);
                    Debug.Log(d[j].text);
                    for (int k = 0; k < d[j].answers.Count; k++) Debug.Log(d[j].answers[k]);
                }
            }

            for(int i = 0; i < dialogues.Count; i++)
            {
                var nodes = dialogues[i];
                for(int j = 0; j < nodes.Count; j++)
                {
                    var node = nodes[j];
                    //node.
                    //foreach (var port in node.Ports)
                    //{
                    //    port.
                    //}
                }
            }
        }

        private void Import ()
        {
            //string path = EditorUtility.OpenFolderPanel("Load file...", "", "");
            string path = EditorUtility.OpenFilePanel("Select file...", "", "asset");
            path = path.Replace(Application.dataPath, "Assets");

            //string[] files = Directory.GetFiles(path);

            var dialogue = AssetDatabase.LoadAssetAtPath<Dialogue>(path);

            //Debug.Log( Application.persistentDataPath
            if (dialogue != null) Debug.Log("Loaded: " + dialogue, dialogue);

            for (int j = 0; j < dialogue.nodes.Count; j++)
            {
                if (dialogue.nodes[j] is DialogueNode)
                {
                    DialogueNode node = dialogue.nodes[j] as DialogueNode;
                    if (node.character != null && node.character.name == "Tester")
                    {
                        node.AddDynamicOutput(typeof(DialogueNodeBase), XNode.Node.ConnectionType.Override, XNode.Node.TypeConstraint.None, "answers 0");
                    }
                }
            }
        }

        public class Node
        {
            //public List<Node> inputs;
            //public List<Node> outputs;

            //public Vector2 position;

            //public int graph;

            public int id;
            public string text;
            public List<string> answers = new List<string>();
        }

    }
}
