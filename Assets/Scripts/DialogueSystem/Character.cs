﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DialogueSystem
{
    [CreateAssetMenu(menuName = "DialogueSystem/Character")]
    public class Character : ScriptableObject
    {
        public Color color;
    }
}