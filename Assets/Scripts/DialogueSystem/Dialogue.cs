﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XNode;

namespace DialogueSystem
{
    [CreateAssetMenu(menuName = "DialogueSystem/Dialogue", order = 0)]
    public class Dialogue : NodeGraph
    {
        [HideInInspector]
        public DialogueNode current;

        public void Restart()
        {
            //Find the first DialogueNode without any inputs. This is the starting node.
            var entry = nodes.Find(x => x is EntryNode && (x as EntryNode).startPoint) as EntryNode;
            if (entry == null) entry = nodes.Find(x => x is EntryNode) as EntryNode;

            if (entry != null) entry.Trigger();
            else if (nodes.Count > 0)
            {
                for(int i = 0; i < nodes.Count; i++)
                {
                    if(nodes[i] is DialogueNode)
                    {
                        current = nodes[i] as DialogueNode;
                        return;
                    }
                }
            }
        }

        public void Restart(EntryNode entry)
        {
            if (entry != null) entry.Trigger();
            else if (nodes.Count > 0)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (nodes[i] is DialogueNode)
                    {
                        current = nodes[i] as DialogueNode;
                        return;
                    }
                }
            }

        }
        public DialogueNode Select (int i)
        {
            current.Select(i);
            return current;
        }

        public bool IsReady ()
        {
            return current != null;
        }

        public void Serialize ()
        {
            for(int i = 0; i < nodes.Count; i++)
            {

            }
        }
    }
}
