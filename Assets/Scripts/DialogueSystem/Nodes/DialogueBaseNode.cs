﻿using XNode;

namespace DialogueSystem
{
    public abstract class DialogueNodeBase : Node
    {
        [Input(backingValue = ShowBackingValue.Never, typeConstraint = TypeConstraint.Inherited)] public DialogueNodeBase input;
        [Output(backingValue = ShowBackingValue.Never)] public DialogueNodeBase output;

        abstract public void Trigger();

        public override object GetValue(NodePort port)
        {
            return null;
        }
    }
}
