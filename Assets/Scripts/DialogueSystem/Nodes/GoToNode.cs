﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace DialogueSystem
{
    [NodeTint("#CCFFCC"), NodeWidth(208)]
    public class GoToNode : DialogueNodeBase
    {
        public EntryNode entry;

        public override void Trigger()
        {
            GMATools.Common.EventManager.Instance().TriggerEvent("GoToDialogue", entry);
        }
    }
}
