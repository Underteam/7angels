﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace DialogueSystem
{
    [NodeTint("#FFFFAA")]
    public class Action : DialogueNodeBase
    {
        public GameAction action;

        public Object[] parameters;

        public SerializableEvent[] trigger; // Could use UnityEvent here, but UnityEvent has a bug that prevents it from serializing correctly on custom EditorWindows. So i implemented my own.

        public override void Trigger()
        {
            if (action == null) return;

            if (action.useEvents)
            {
                for (int i = 0; i < trigger.Length; i++)
                {
                    trigger[i].Invoke();
                }
            }
            else if(action.useParameters)
            {
                GMATools.Common.EventManager.Instance().TriggerEvent(action.actionName, parameters);
            }
        }
    }
}