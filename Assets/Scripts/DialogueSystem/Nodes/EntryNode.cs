﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace DialogueSystem
{
    [NodeTint("#CCFFCC"), NodeWidth(208)]
    public class EntryNode : DialogueNodeBase
    {
        public string entryName;

        public bool startPoint;

        public override void Trigger()
        {
            NodePort port = GetOutputPort("output");

            if (port == null) return;

            for (int i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);
                (connection.node as DialogueNodeBase).Trigger();
            }
        }
    }
}
