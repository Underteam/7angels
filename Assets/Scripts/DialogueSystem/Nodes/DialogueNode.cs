﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace DialogueSystem
{
    [NodeTint("#CCFFCC")]
    public class DialogueNode : DialogueNodeBase
    {
        public Character character;
        [TextArea] public string text;
        public Sprite image;
        [Output(dynamicPortList = true)] public List<Answer> answers = new List<Answer>();

        [System.Serializable]
        public class Answer
        {
            [TextArea] public string text;
            public int cost;
            public AudioClip voiceClip;
        }

        public void Select (int index)
        {
            NodePort port = null;
            if (answers.Count == 0)
            {
                port = GetOutputPort("output");
                //Debug.Log("Select " + index + " next", this);
            }
            else
            {
                if (answers.Count <= index) return;
                port = GetOutputPort("answers " + index);
                //Debug.Log("Select " + index + " " + answers[index].text, this);
            }

            if (port == null)
            {
                Debug.Log("Output not found", this);
                return;
            }

            for (int i = 0; i < port.ConnectionCount; i++)
            {
                NodePort connection = port.GetConnection(i);
                (connection.node as DialogueNodeBase).Trigger();
            }
        }

        public override void Trigger()
        {
            (graph as Dialogue).current = this;
        }
    }
}