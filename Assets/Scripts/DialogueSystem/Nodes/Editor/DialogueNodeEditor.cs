﻿using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;
using DialogueSystem;

namespace DialogueSystemEditor
{
    [CustomNodeEditor(typeof(DialogueNode))]
    public class DialogueNodeEditor : NodeEditor
    {

        public override void OnBodyGUI()
        {
            serializedObject.Update();

            DialogueNode node = target as DialogueNode;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("character"), GUIContent.none);
            if (node.answers.Count == 0)
            {
                GUILayout.BeginHorizontal();
                NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"), GUILayout.MinWidth(0));
                NodeEditorGUILayout.PortField(GUIContent.none, target.GetOutputPort("output"), GUILayout.MinWidth(0));
                GUILayout.EndHorizontal();
            }
            else
            {
                NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"));
            }
            GUILayout.Space(-30);

            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("text"), GUIContent.none, true, GUILayout.Width(200), GUILayout.MinHeight(80));

            GUILayout.Space(-65);
            DialogueNode thisNode = target as DialogueNode;
            thisNode.image = (Sprite)EditorGUILayout.ObjectField("", thisNode.image, typeof(Sprite), false);

            NodeEditorGUILayout.DynamicPortList("answers", typeof(DialogueNodeBase), serializedObject, NodePort.IO.Output, Node.ConnectionType.Override);

            serializedObject.ApplyModifiedProperties();
        }

        public override int GetWidth()
        {
            return 300;
        }

        public override Color GetTint()
        {
            DialogueNode node = target as DialogueNode;
            if (node.character == null) return base.GetTint();
            else
            {
                Color col = node.character.color;
                col.a = 1;
                return col;
            }
        }
    }
}