﻿using System.Collections.Generic;
using System.Linq;
using DialogueSystem;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace DialogueSystemEditor
{
    [CustomNodeEditor(typeof(DialogueSystem.Action))]
    public class ActionEditor : NodeEditor
    {
        public override void OnBodyGUI()
        {
            serializedObject.Update();

            ////Event node = target as Event;
            NodeEditorGUILayout.PortField(target.GetInputPort("input"), GUILayout.Width(100));
            EditorGUILayout.Space();

            var prop = serializedObject.FindProperty("action");
            EditorGUILayout.PropertyField(prop, GUIContent.none);

            //if (prop != null && prop.objectReferenceValue != null)
            //{
            //    var o = new SerializedObject(prop.objectReferenceValue);

            //    //Debug.Log(o.FindProperty("trigger").displayName + " " + prop.FindPropertyRelative("trigger"));

            //    string pName = (prop.objectReferenceValue as GameAction).GetPropertyName();
            //    if (!string.IsNullOrEmpty(pName))
            //    {
            //        NodeEditorGUILayout.PropertyField (o.FindProperty(pName));
            //        o.ApplyModifiedProperties();
            //    }
            //}

            if(prop != null && (prop.objectReferenceValue as DialogueSystem.GameAction) != null)
            {
                if ((prop.objectReferenceValue as DialogueSystem.GameAction).useEvents) NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("trigger"));
                if ((prop.objectReferenceValue as DialogueSystem.GameAction).useParameters) NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("parameters"));
            }

            serializedObject.ApplyModifiedProperties();
        }

        public override int GetWidth()
        {
            return 336;
        }
    }
}