﻿using System.Collections.Generic;
using System.Linq;
using DialogueSystem;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace DialogueSystemEditor
{
    [CustomNodeEditor(typeof(DialogueSystem.EntryNode))]
    public class EntryNodeEditor : NodeEditor
    {
        List<string> names = new List<string>();

        public override void OnBodyGUI()
        {
            serializedObject.Update();

            EntryNode thisEntry = target as EntryNode;

            NodeEditorGUILayout.PortField(GUIContent.none, target.GetOutputPort("output"), GUILayout.MinWidth(0));
            //EditorGUILayout.Space();

            EditorGUI.BeginChangeCheck();
            var prop = serializedObject.FindProperty("entryName");
            GUILayout.Space(-18);
            EditorGUILayout.PropertyField(prop, GUIContent.none);
            if(EditorGUI.EndChangeCheck() || string.IsNullOrEmpty(prop.stringValue))
            {
                string thisEntryName = prop.stringValue;

                names.Clear();
                //List<Entry> entries = new List<Entry>();

                string[] guids = AssetDatabase.FindAssets("t:Dialogue");
                for (int i = 0; i < guids.Length; i++)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                    Dialogue dialogue = AssetDatabase.LoadAssetAtPath(path, typeof(Dialogue)) as Dialogue;
                    if (dialogue != null)
                    {
                        for (int j = 0; j < dialogue.nodes.Count; j++)
                        {
                            if (dialogue.nodes[j] is EntryNode)
                            {
                                EntryNode entry = dialogue.nodes[j] as EntryNode;
                                if (entry == null || entry == thisEntry) continue;
                                if (!names.Contains(entry.entryName)) names.Add(entry.entryName);
                                //entries.Add(entry);
                            }
                        }
                    }
                }

                //Debug.Log("Found " + names.Count + " entries (" + thisEntryName + ")", thisEntry);
                //for (int i = 0; i < names.Count; i++) Debug.Log(names[i]);

                if (names.Contains(thisEntryName))
                {
                    string digits = "0123456789";
                    int i = thisEntryName.Length - 1;
                    while (i >= 0 && digits.Contains(thisEntryName[i])) i--;

                    string prefix = "";
                    int num = 0;

                    if (i >= 0)
                    {
                        prefix = thisEntryName.Substring(0, i + 1);
                        string strNum = thisEntryName.Substring(i+1, thisEntryName.Length - i - 1);
                        //Debug.Log(strNum);
                        int.TryParse(strNum, out num);
                    }

                    while (names.Contains(prefix + num)) num++;

                    prop.stringValue = prefix + num;
                }

                if (prop.stringValue == "") prop.stringValue = "entry" + names.Count;
            }

            //EditorGUI.BeginChangeCheck();
            prop = serializedObject.FindProperty("startPoint");
            //GUILayout.Space(-18);
            EditorGUILayout.PropertyField(prop, new GUIContent("Start Point"));
            //if (EditorGUI.EndChangeCheck())
            {
                List<EntryNode> nodes = new List<EntryNode>();
                for(int i = 0; i < thisEntry.graph.nodes.Count; i++)
                {
                    EntryNode node = thisEntry.graph.nodes[i] as EntryNode;
                    if (node != null &&  node != thisEntry && node.startPoint)
                    {
                        nodes.Add(node);
                    }
                }

                if(prop.boolValue || nodes.Count == 0)
                {
                    for (int i = 0; i < nodes.Count; i++) nodes[i].startPoint = false;
                    prop.boolValue = true;
                }
                else if(nodes.Count > 1)
                {
                    for (int i = 1; i < nodes.Count; i++) nodes[i].startPoint = false;
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}