﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using DialogueSystem;

namespace DialogueSystemEditor
{
    // prefab override logic works on the entire property.
    [CustomPropertyDrawer(typeof(EntryNode))]
    public class EntryNodeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();

            // Store old indent level and set it to 0, the PrefixLabel takes care of it

            position = EditorGUI.PrefixLabel(position, label);

            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            Rect buttonRect = position;
            //buttonRect.width = 160;

            string buttonLabel = "Select";
            EntryNode currentEntry = property.objectReferenceValue as DialogueSystem.EntryNode;
            if (currentEntry != null) buttonLabel = currentEntry.entryName;
            if (GUI.Button(buttonRect, buttonLabel))
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("None"), currentEntry == null, () => Apply(property, null));
                var list = GetEntryNodes();
                for (int i = 0; i < list.Count; i++)
                {
                    EntryNode entry = list[i];
                    if (entry != null)
                    {
                        GUIContent content = new GUIContent(entry.entryName);
                        string[] nameParts = entry.entryName.Split(' ');
                        if (nameParts.Length > 1) content.text = nameParts[0] + "/" + entry.entryName.Substring(nameParts[0].Length + 1);
                        menu.AddItem(content, entry == currentEntry, () => Apply(property, entry));
                    }
                }
                menu.ShowAsContext();
            }

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        private void Apply(SerializedProperty property, DialogueSystem.EntryNode node)
        {
            property.objectReferenceValue = node;
            property.serializedObject.ApplyModifiedProperties();
            property.serializedObject.Update();
        }

        private List<EntryNode> nodes = new List<EntryNode>();
        private List<EntryNode> GetEntryNodes ()
        {
            nodes.Clear();
            string[] guids = AssetDatabase.FindAssets("t:Dialogue");
            for (int i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                Dialogue dialogue = AssetDatabase.LoadAssetAtPath(path, typeof(Dialogue)) as Dialogue;
                if (dialogue != null)
                {
                    for (int j = 0; j < dialogue.nodes.Count; j++)
                    {
                        if (dialogue.nodes[j] is EntryNode)
                        {
                            EntryNode entry = dialogue.nodes[j] as EntryNode;
                            if (entry == null) continue;
                            if (!nodes.Contains(entry)) nodes.Add(entry);
                        }
                    }
                }
            }

            return nodes;
        }
    }
}