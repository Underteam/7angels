﻿using System.Collections.Generic;
using System.Linq;
using DialogueSystem;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace DialogueSystemEditor
{
    [CustomNodeEditor(typeof(DialogueSystem.GoToNode))]
    public class GoToNodeEditor : NodeEditor
    {
        public override void OnBodyGUI()
        {
            serializedObject.Update();

           // EntryNode thisEntry = target as EntryNode;

            NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"), GUILayout.MinWidth(0));

            var prop = serializedObject.FindProperty("entry");
            GUILayout.Space(-18);
            EditorGUILayout.PropertyField(prop, GUIContent.none);

            serializedObject.ApplyModifiedProperties();
        }
    }
}