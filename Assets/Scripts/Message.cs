﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Message : MonoBehaviour {

    public Text lblMessage;

    public Image imgImage;

    public Text lblTime;

    private RectTransform imageRT;

    public RectTransform baseRT;

	// Use this for initialization
	void Start () {
		
	}

    private void Update()
    {
        if (!Application.isPlaying)
        {
            if (imageRT == null && imgImage != null) imageRT = imgImage.transform as RectTransform;

            if (imgImage != null && imgImage.sprite != null)
            {
                float w = imgImage.sprite.rect.width;
                float h = imgImage.sprite.rect.height;

                var size = imageRT.sizeDelta;
                size.y = size.x * h / w;
                imageRT.sizeDelta = size;
            }

            ApplySize();
        }
    }

    public void Apply (string message, Sprite image)
    {
        lblMessage.text = message;
        if (imgImage != null)
        {
            imgImage.sprite = image;
            if (image == null) imgImage.gameObject.SetActive(false);
            else imgImage.gameObject.SetActive(true);
        }

        if (string.IsNullOrEmpty(message)) lblMessage.gameObject.SetActive(false);
        else lblMessage.gameObject.SetActive(true);

        ApplySize();
    }

    private void ApplySize ()
    {
        var size = baseRT.sizeDelta;

        size.y = 0;
        size.y += (lblMessage.transform as RectTransform).rect.height;
        size.y += imgImage == null || imgImage.sprite == null ? 0 : (imgImage.transform as RectTransform).rect.height;
        size.y += (lblTime.transform as RectTransform).rect.height;

        float h = size.y + 50;

        baseRT.sizeDelta = size;

        var rt = (transform as RectTransform);
        size = rt.sizeDelta;
        size.y = h;
        rt.sizeDelta = size;
    }
}
