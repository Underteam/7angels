﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ISaveableType {

    public GameObject target;

    public List<ISaveable> GetInterfaces ()
    {
        List<ISaveable> interfaces = new List<ISaveable>();

        if (target == null) return interfaces;

        var list = target.GetComponents<Component>();

        for(int i = 0; i < list.Length; i++)
        {
            if (list[i] is ISaveable) interfaces.Add(list[i] as ISaveable);
        }

        return interfaces;
    }
}
